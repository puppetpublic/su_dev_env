# Install java for development purposes

class su_dev_env::java(
  $include_generic = true,
) {

  if ($include_generic) {
    include su_dev_env::generic
  }

  case $::lsbdistcodename {
    'precise':  { include java::v6 }
    'oneiric':  { include java::v6 }
    'natty':    { include java::v6 }
    'karmic':   { include java::v6 }
    'lucid':    { include java::v6 }
    'tikanga':  { include java::v6 }
    'santiago': { include java::v6 }
    'squeeze':  { include java::v6 }
    'wheezy':   { include packages::java_7 }
    'jessie':   { include packages::java_7 }
    'stretch':  { include packages::java_8 }
    'buster':   { include packages::java_11 }
    'bullseye': { include packages::java_11 }
    'bookworm': { include packages::java_17 }
    default:    { include java::v5 }
  }

}
